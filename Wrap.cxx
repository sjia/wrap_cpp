#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <string>

int main( int argc, char ** argv )
{
  const int Dimension = 3;
  
  //Parsing initial parameters
  struct Param param;
  parseParameters( argc, argv, param);
   
  //Definition of the type:
  typedef double                                PixelType;
  
  typedef itk::Index<Dimension>                 IndexType;
  typedef itk::Vector<PixelType,Dimension>      VectorPixelType;
  typedef itk::Image<VectorPixelType,Dimension> VectorImageType;	
  typedef itk::Image<PixelType,Dimension>       ImageType;
  
  //typedef itk::PointSet<PixelType, Dimension>       PointSetType;
  typedef itk::Mesh< PixelType, Dimension >     MeshType;
  typedef MeshType::PointType                   PointType;

  
  //Definition of Reader and Writer Classes:
  typedef itk::ImageFileReader< VectorImageType >  VectorReaderType;
  typedef itk::ImageFileWriter< VectorImageType >  VectorWriterType;
  typedef itk::ImageFileReader< ImageType >  ReaderType;
  typedef itk::ImageFileWriter< ImageType >  WriterType;
  typedef itk::MeshFileReader< MeshType >  MeshReaderType;
  typedef itk::MeshFileWriter< MeshType >  MeshWriterType;

  
  // Filters
  typedef itk::MultiplyImageFilter<VectorImageType,ImageType,VectorImageType>            MultiplierType;
  typedef itk::ExponentialDisplacementFieldImageFilter<VectorImageType, VectorImageType> ExpFilterType;
  typedef itk::WarpImageFilter<ImageType,ImageType,VectorImageType>                      WarperType;
  typedef itk::InvertDisplacementFieldImageFilter<VectorImageType, VectorImageType>      InverserType;

  
  // Field
  typename VectorReaderType::Pointer vreader  = VectorReaderType::New();
  typename VectorWriterType::Pointer vwriter  = VectorWriterType::New();
  typename VectorImageType::Pointer field;
  
  vreader->SetFileName(param.InputSvf);
  vreader->Update();

  ///////////////////////////////////////////////////////////////////////
  // Transformation 
  if(param.isDisplacement)
    {
      if (param.computeInverse)
	{
	  typename InverserType::Pointer inverser = InverserType::New(); 
	  inverser->SetDisplacementField(vreader->GetOutput());
	  inverser->SetMaxErrorToleranceThreshold(0.1);
	  inverser->SetMeanErrorToleranceThreshold(0.01);
	  inverser->SetEnforceBoundaryCondition(false);
	  inverser->Update();
	  field=inverser->GetOutput();
	}
      else
	{
	  field=vreader->GetOutput();
	}

    }
  else
    {
      // Rescaling
      typename MultiplierType::Pointer multiplier = MultiplierType::New();
      multiplier->SetInput(vreader->GetOutput());
      multiplier->SetConstant((param.computeInverse?-1.:+1.) * param.Scaling);
      
      // Exponentialisation
      typename ExpFilterType::Pointer exper = ExpFilterType::New();
      exper->SetInput(multiplier->GetOutput());     
      exper->Update();
      field=exper->GetOutput();
    }

  if (param.OutTransform != "")
    writeImage<VectorImageType>(field,vwriter,param.OutTransform);
  if (param.InputImage == "")
    return 0;
  
  ///////////////////////////////////////////////////////////////////////
  // Point set or Image & warper
  if (param.isMesh)
    {
      MeshReaderType::Pointer mreader = MeshReaderType::New();
      MeshWriterType::Pointer mwriter = MeshWriterType::New();

      mreader->SetFileName( param.InputImage );
      mreader->Update();

      typename MeshType::Pointer mesh = mreader->GetOutput();

      // petite parenthèse 
      unsigned int numberOfPoints = mesh->GetNumberOfPoints();
      unsigned int numberOfCells  = mesh->GetNumberOfCells();
      std::cout << "numberOfPoints = " << numberOfPoints << std::endl;
      std::cout << "numberOfCells  = " << numberOfCells << std::endl;
 
      /* Retrieve points
      for(unsigned int i = 0; i < numberOfPoints; i++)
	{
	  PointType pp;
	  bool pointExists = mesh->GetPoint(i, &pp);
	  if(pointExists) 
	    std::cout << "Point is = " << pp << std::endl;
	}
      */ 

      // Transformation
      typedef itk::DisplacementFieldTransform< PixelType, Dimension >  DisplacementFieldTransformType;
      typedef itk::TransformMeshFilter< MeshType, MeshType, DisplacementFieldTransformType > MeshFilterType;
      DisplacementFieldTransformType::Pointer displacementFieldTransform = DisplacementFieldTransformType::New();
      displacementFieldTransform->SetDisplacementField( field );
      MeshFilterType::Pointer filter = MeshFilterType::New();

      // Warpings
      if (param.NbSteps >1)
	{
	  std::string filename = param.OutputImage+to_string(0)+".vtk";
	  mwriter->SetFileName( filename );
	  mwriter->SetInput( mesh );
	  mwriter->Update();
	  std::cout<<"Writing mesh in "<< filename <<std::endl;
	}
      for(int i=1;i<=param.NbSteps;i++)
	{
	  filter->SetInput( mesh );
	  filter->SetTransform( displacementFieldTransform );
	  filter->Update();
	  mesh=filter->GetOutput();
	  mesh->DisconnectPipeline();

	  std::string filename = (param.NbSteps > 1)?param.OutputImage+to_string(i)+".vtk":param.OutputImage;
	  mwriter->SetFileName( filename );
	  mwriter->SetInput( mesh );
	  mwriter->Update();
	  std::cout<<"Writing mesh in "<< filename <<std::endl;
	}
    }
  else
    {
      typename ReaderType::Pointer reader         = ReaderType::New();
      typename WriterType::Pointer writer         = WriterType::New();
      
      reader->SetFileName(param.InputImage);
      reader->Update();
      
      //writer->SetImageIO(reader->GetImageIO());
      
      typename ImageType::Pointer image   = reader->GetOutput();
      typename WarperType::Pointer warper = WarperType::New();
  
      warper->SetOutputOrigin(   field->GetOrigin());
      warper->SetOutputSpacing(  field->GetSpacing());
      warper->SetOutputDirection(field->GetDirection());

      // Warpings
      if (param.NbSteps >1)
	{
	  std::string filename=param.OutputImage+to_string(0)+".vtk";
	  writeImage<ImageType>(image,writer,filename);
	}
      for(int i=1;i<=param.NbSteps;i++)
	{
	  warper->SetDisplacementField(field);
	  warper->SetInput(image);
	  warper->Update();
	  image = warper->GetOutput();
	  image->DisconnectPipeline();

	  std::string filename = (param.NbSteps > 1)?param.OutputImage+to_string(i)+".vtk":param.OutputImage;
	  writeImage<ImageType>(image,writer,filename);
	}
    }
  return EXIT_SUCCESS;
}

